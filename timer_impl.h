#include "timer.h"

extern Reg32 regTIM2_CNT;
extern Reg32 regTIM2_SR;
extern Reg32 regTIM2_CR1;
extern Reg32 regNVIC_ISER0;
extern Reg32 regTIM2_DIER;

uint16_t timer_start(void);
uint16_t timer_stop(uint16_t);
void timer_shutdown(void);
