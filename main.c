#include <stdio.h>
#include "main.h"




int main()
{
	//initialization
	setupRegs();
	
	open_led();
  open_serial();
	timer_ini();
	timer_4_ini();
	adc_ini();
	adc_interrupt_ini();
	* regADC1_CR2 |=0x40000;
	while(1){
	}

	return 0;
}
void ADC1_2_IRQHandler(void){
	
	uint16_t recorder = * regADC1_SR;
	if(recorder & 0x2){
		adc_val = * regADC1_DR;
		
	}
	
}

void USART2_IRQHandler(void){
	uint8_t data;
	//receive command first
	data = getByte();
	//if not type in "enter", keep reading 
	if(data != 0x0D){
		//if not receiving backspace, record command, and send the byte that user types in
		if(data != 0x7F){
				inBuffer[inBufferCounter]=data;
				sendByte(data);
				inBufferCounter++;
			}
		//if receving backspace, record noting, and move pointer backward. 
			else{
				sendByte(data);
				if(inBufferCounter != 0){
					inBufferCounter--;
				}
			}
	}
	else{
		sendByte('\r');
		sendByte('\n');
		//when enter the "enter", run afterward function, and clear the pointer
		run();
		inBufferCounter = 0;
		sendByte('\r');
		sendByte('\n');
	}
}

