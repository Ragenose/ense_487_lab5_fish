#include "adc.h"

void adc_ini(void){
	* regRCC_APB2ENR |= 0x10; 					  //enable GPIOC
	* regGPIOC_CRL	 &= ~0xf;		 //clean up CNF0, MODE0;
	* regGPIOC_CRL	 |= 0x4; 					 //GPIOC PC0 set as 0100 as floating input
	* regRCC_APB2ENR |= 0x200;					 //enable ADC1
	* regADC1_SQR3 	|=	0x0000000A;			//channel 10 in ADC1 
	* regADC1_SMPR1 |= 0x5;							//channel 10, 55.5 cycle
	* regADC1_CR1		|= 0x120;
	* regADC1_CR2		|= 0xE0001;					//set external trigger high;
	
	* regADC1_CR2		|= 0x4;							//enabling calibaration		
	while(* regADC1_CR2 & 0x2);
	* regADC1_CR2		|= 0x2;							//continuous conversion
}

void adc_interrupt_ini(void){
	* regNVIC_ISER0 |=0x40000;  //#18
}