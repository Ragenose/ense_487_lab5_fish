#include "registers.h"
#include "util.h"
#include "led.h"
#include "serial.h"
#include "cli.h"
#include "timer_4_ini.h"
#include "adc.h"

extern Reg32 regRCC_APB2ENR;
extern Reg32 regGPIOB_ODR;
extern Reg32 regGPIOB_CRH;
extern Reg32 regGPIOB_BSRR;
extern Reg32 regGPIOB_BRR;

extern uint8_t inBuffer[30];
extern uint8_t outBuffer[30];
extern int inBufferCounter;
extern int outBufferCounter;

extern uint8_t command_led[];
extern uint8_t command_date[];
extern uint8_t command_time[];
extern uint8_t tempBuffer[BUFFER_SIZE];
extern int tempBufferCounter;

uint32_t adc_val;