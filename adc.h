#include "util.h"
#include "registers.h"

extern Reg32 regGPIOC_CRL;
extern Reg32 regRCC_APB2ENR;
extern Reg32 regADC1_SR;
extern Reg32 regADC1_CR1;
extern Reg32 regADC1_CR2;
extern Reg32 regADC1_SMPR1;
extern Reg32 regADC1_SQR3;
extern Reg32 regADC1_DR;
extern Reg32 regNVIC_ISER0;

void adc_ini(void);
void adc_interrupt_ini(void);