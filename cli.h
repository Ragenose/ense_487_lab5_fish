#include "serial.h"
#include "led.h"
#include "registers.h"
#include "timer_impl.h"
#include "math.h"
#include "timer_4_ini.h"

//first input buffer to store command that user types in.
extern uint8_t inBuffer[BUFFER_SIZE];
extern uint8_t outBuffer[BUFFER_SIZE];
//pointer to input buffer
extern int inBufferCounter;
extern int outBufferCounter;
extern uint32_t adc_val;

//commnd type, use for LED,LED #, LED operation, time, and date function 
typedef enum{
	help,
	led,
	date,
	time,
	invalid,
	on,
	off,
	status,
	all,
	led_0,
	led_1,
	led_2,
	led_3,
	led_4,
	led_5,
	led_6,
	led_7,
	timer,
	pwm,
	adc
}Command;





int getSize(void);
uint8_t checkCommand(uint8_t [], int);
Command getFirstArgu(void);
Command getNextArgu(void);

Command checkOperation(void);
void run(void);

void operation_led(Command, Command);
void operation_date(void);
void operation_time(void);
void operation_invalid(void);
void operation_led_status(uint8_t);
void operation_help(void);
void operation_timer(void);
void operation_pwm(void);
void operation_adc(void);