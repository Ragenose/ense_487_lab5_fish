#include "stdint.h"
#include "registers.h"

extern Reg32 regRCC_APB1ENR;
extern Reg32 regTIM2_CR1;
extern Reg32 regTIM2_ARR;
extern Reg32 regTIM2_PSC;

void timer_ini(void);

/*
8Byte structure
2*32bit
*/
typedef struct{
	uint32_t random[2];
}struct_8;
 
/*
128 Byte structure
32*32bit
*/
typedef struct{
	uint32_t random[32];
}struct_128;


uint32_t random_32(void);
uint64_t random_64(void);

uint16_t add(int);
uint16_t multiply(int);
uint16_t divide(int);
uint16_t structure_8(void);
uint16_t structure_128(void);
uint16_t structure_1024(void);
