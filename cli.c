#include "cli.h"

uint8_t command_led[]="led";
uint8_t command_date[]="date";
uint8_t command_time[]="time";
uint8_t command_help[]="help";
uint8_t command_timer[]="timer";
uint8_t command_pwm[]="pwm";
uint8_t command_adc[]="adc";

uint8_t argu_on[]="on";
uint8_t argu_off[]="off";
uint8_t argu_all[]="all";
uint8_t argu_status[]="status";

uint8_t argu_led0[]={'0'};
uint8_t argu_led1[]={'1'};
uint8_t argu_led2[]={'2'};
uint8_t argu_led3[]={'3'};
uint8_t argu_led4[]={'4'};
uint8_t argu_led5[]={'5'};
uint8_t argu_led6[]={'6'};
uint8_t argu_led7[]={'7'};

uint8_t tempBuffer[BUFFER_SIZE];
//first pointer that will keep reading from the beginning to the end of the command
int tempBufferCounter;
//second pointer that will start with the first "space" that user types in, and keep reading the remaining command after "space"
int arguCounter;

//reading the first argument before and "space" or "enter"
Command getFirstArgu(void){
	for(tempBufferCounter =0;tempBufferCounter<inBufferCounter;tempBufferCounter++){
		if(inBuffer[tempBufferCounter] != ' ' && inBuffer[tempBufferCounter] != 0x00){
			tempBuffer[tempBufferCounter] = inBuffer[tempBufferCounter];
		}
		else{
			break;
		}
	}
	//send the buffer result the next function
	return checkOperation();
}

//if start with led, then go to this function, it will read the number of led, and the operation of led
Command getNextArgu(void){
	arguCounter=0;
	for(;tempBufferCounter<inBufferCounter;tempBufferCounter++){
		if(inBuffer[tempBufferCounter] != ' '){
			break;
		}
	}
	for(;tempBufferCounter<inBufferCounter;tempBufferCounter++,arguCounter++){
		if(inBuffer[tempBufferCounter] != ' ' && inBuffer[tempBufferCounter] != 0x00){
			tempBuffer[arguCounter] = inBuffer[tempBufferCounter];
		}
		else{
			break;
		}
	}
	return checkOperation();
}


//compare the command size, then compare each character inside the buffer, and return true of false to the corresponding function in checkOperation()
uint8_t checkCommand(uint8_t command[],int size){
	if((tempBufferCounter != size) && (arguCounter != size)){
		return 0;
	}
	for(int i = 0;i<size;i++){
		if(tempBuffer[i] != command[i]){
			return 0;
		}
	}
	return 1;
}


//compare the size first, and then the character inside the buffer, if any command is totally matched, it return correspoding enum(command)
Command checkOperation(void){
	if(checkCommand(command_led,3)){
		return led;
	}
	else if(checkCommand(command_help,4)){
		return help;
	}
	else if(checkCommand(command_date,4)){
		return date;
	}
	else if(checkCommand(command_time,4)){
		return time;
	}
	else if(checkCommand(command_timer,5)){
		return timer;
	}
	else if(checkCommand(command_pwm,3)){
		return pwm;
	}
	else if(checkCommand(command_adc,3)){
		return adc;
	}
	else if(checkCommand(argu_on,2)){
		return on;
	}
	else if(checkCommand(argu_off,3)){
		return off;
	}
	else if(checkCommand(argu_all,3)){
		return all;
	}
	else if(checkCommand(argu_status,6)){
		return status;
	}
	else if(checkCommand(argu_led0,1)){
		return led_0;
	}
	else if(checkCommand(argu_led1,1)){
		return led_1;
	}
	else if(checkCommand(argu_led2,1)){
		return led_2;
	}
	else if(checkCommand(argu_led3,1)){
		return led_3;
	}
	else if(checkCommand(argu_led4,1)){
		return led_4;
	}
	else if(checkCommand(argu_led5,1)){
		return led_5;
	}
	else if(checkCommand(argu_led6,1)){
		return led_6;
	}
	else if(checkCommand(argu_led7,1)){
		return led_7;
	}
	else{
		return invalid;
	}
}
//check first argument to detect what to do
//if LED, then it should read the next command, # of led and the opeartion of led
void run(void){
	Command command=getFirstArgu();
	switch(command){
		//if led, then keep reading the next argument after "space"
		case led:
			Command arg = getNextArgu();
			Command number = getNextArgu();
			operation_led(arg,number);
			break;
		case date:
			operation_date();
			break;
		case time:
			operation_time();
			break;
		case help:
			operation_help();
			break;
		case timer:
			operation_timer();
			break;
		case pwm:
			getNextArgu();
			operation_pwm();
			break;
		case adc:
			operation_adc();
			break;
		default:
			operation_invalid();
			break;
	}
}

//operation of leds
void operation_led(Command command, Command number){
	//switch statement to check which led or all
	switch(number){
				case led_0:
					//check command for led, on to turn on led, off to turn off led and status to check led status
					switch(command){
						case on:
							turn_on_led(0);
							break;
						case off:
							turn_off_led(0);
							break;
						case status:
							operation_led_status(0);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_1:
					switch(command){
						case on:
							turn_on_led(1);
							break;
						case off:
							turn_off_led(1);
							break;
						case status:
							operation_led_status(1);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_2:
					switch(command){
						case on:
							turn_on_led(2);
							break;
						case off:
							turn_off_led(2);
							break;
						case status:
							operation_led_status(2);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_3:
					switch(command){
						case on:
							turn_on_led(3);
							break;
						case off:
							turn_off_led(3);
							break;
						case status:
							operation_led_status(3);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_4:
					switch(command){
						case on:
							turn_on_led(4);
							break;
						case off:
							turn_off_led(4);
							break;
						case status:
							operation_led_status(4);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_5:
					switch(command){
						case on:
							turn_on_led(5);
							break;
						case off:
							turn_off_led(5);
							break;
						case status:
							operation_led_status(5);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_6:
					switch(command){
						case on:
							turn_on_led(6);
							break;
						case off:
							turn_off_led(6);
							break;
						case status:
							operation_led_status(6);
							break;
						default:
							operation_invalid();
					}
					break;
				case led_7:
					switch(command){
						case on:
							turn_on_led(7);
							break;
						case off:
							turn_off_led(7);
							break;
						case status:
							operation_led_status(7);
							break;
						default:
							operation_invalid();
					}
						break;
					case all:
						switch(command){
							case on:
								for(int i=0;i<8;i++){
									turn_on_led(i);
								}
								break;
							case off:
								for(int i=0;i<8;i++){
									turn_off_led(i);
								}
								break;
							case status:
								if(number == all){
									for(int i=0;i<8;i++){
										operation_led_status(i);
									}
								}
								break;
							default:
								operation_invalid();
						}
						break;
					default:
						operation_invalid();
						break;
				}
}

//Output invalid command when doesn't recognize input
void operation_invalid(void){
	sendString("Invalid command",(int)(sizeof("Invalid command")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
}

//Check selected led's status
void operation_led_status(uint8_t led){
	//Get led status
	uint8_t state = getLedStatus(led);
	//state->0 is off, state->1 is on
	switch(state){
		case 0:
			//output status to terminal
			sendString("LED ",(int)(sizeof("LED ")/sizeof(uint8_t)));
		  sendByte(0x30+led);
			sendString(" state: OFF",(int)(sizeof(" state: OFF")/sizeof(uint8_t)));
			sendByte('\r');
			sendByte('\n');
			break;
		case 1:
			sendString("LED ",(int)(sizeof("LED ")/sizeof(uint8_t)));
		  sendByte(0x30+led);
			sendString(" state: ON",(int)(sizeof(" state: ON")/sizeof(uint8_t)));
			sendByte('\r');
			sendByte('\n');
			break;
	}
}

//Print compilation time
void operation_time(void){
	sendString(__TIME__,(int)(sizeof(__TIME__)/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
}

//Print compilation date
void operation_date(void){
	sendString(__DATE__,(int)(sizeof(__DATE__)/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
}

//Display operation help manual
void operation_help(){
	sendString("List of Command:",(int)(sizeof("List of Command:")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("Date: Print compilation date",(int)(sizeof("Date: Print compilation date")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("	Usage: date",(int)(sizeof("	Usage: date")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("Time: Print compilation time",(int)(sizeof("Time: Print compilation time")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("	Usage: time",(int)(sizeof("	Usage: time")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("LED: Control onboard leds",(int)(sizeof("LED: Control onboard leds")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("	Usage: led [on/off/status] [0-7/all]",(int)(sizeof("	Usage: led [on/off/status] [0-7/all]")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("Timer: measure the average time your processor",(int)(sizeof("Timer: measure the average time your processor")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("	Usage: timer",(int)(sizeof("	Usage: timer")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("PWM: generate pwm signal",(int)(sizeof("PWM: generate pwm signal")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
	sendString("	Usage: pwm [0-100]",(int)(sizeof("	Usage: pwm [0-100]")/sizeof(uint8_t)));
	sendByte('\r');
	sendByte('\n');
}

void operation_timer(void){
	uint16_t result;
	char  data[10];

	sendString("Add 32bit int: ", 15);
	result = add(32);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Add 64bit int: ", 15);
	result = add(64);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');
	
	sendString("Multiply 32bit int: ", 20);
	result = multiply(32);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Multiply 64bit int: ", 20);
	result = multiply(64);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');
	
	sendString("Divide 32bit int: ", 18);
	result = divide(32);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Divide 64bit int: ", 18);
	result = divide(64);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Copy 8 Byte structure: ", 23);
	result = structure_8();
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Copy 128 Byte structure: ", 25);
	result = structure_128();
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');
}

void operation_pwm(void){
	int i;
	int pwm=0;
	char data[10];
	
	if(arguCounter>3 || arguCounter==0){
		operation_invalid();
		return;
	}
	
	//check if characters in the tempbuffer are digit
	for(i=0;i<arguCounter;i++){
		if(tempBuffer[i]<'0' || tempBuffer[i]>'9'){
			operation_invalid();
			return;
		}
	}
	//convert string to integer
	for(i=0;i<arguCounter;i++){
		pwm += (tempBuffer[i]-0x30)*pow(10,arguCounter-i-1);
	}

	//if the percentage is higher than 100, it is an invalid command
	if(pwm >100){
		operation_invalid();
		return;
	}
	* regTIM4_CCR2 = pwm * 100;
}

void operation_adc(void){
	char result[4];
	//convert adc value to string
	sprintf(result,"%d",adc_val);
	sendString(result,4);
	sendByte('\r');
	sendByte('\n');
}

